# getLatestFile takes a directory path and returns the latest file from that directory
import os

def getLatestFile(path)

    files = os.listdir(path)
    latest_file = files[0]
    for key in files:
        if os.path.getctime(path+key) > os.path.getctime(path + latest_file):
            latest_file = key
    return (latest_file)

#print (getLatestFile('/hom/pi/Pictures/CamPhotos/'))