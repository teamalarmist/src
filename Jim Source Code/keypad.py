import RPi.GPIO as GPIO
import time
import datetime #timedelta

# Function getKeypad gets up to 4 values from a 4x4 membrane keypad
# and returns the numbers appended to '*'. Return value = *####
# program allows user 15 seconds to enter password before timing out.

def getKeypad(digits):
    GPIO.setmode (GPIO.BOARD)
    GPIO.setwarnings(False) # remove warnings

    MATRIX = [[ '1', '2', '3', 'A'], # matrix of keypad values
              [ '4', '5', '6', 'B'],
              [ '7', '8', '9', 'C'],
              [ '*', '0', '#', 'D'] ]

    ROW = [ 37, 35, 33, 31] # GPIO pins used

    COL = [ 29, 15, 13, 11] # GPIO pins used

    for j in range (4): # set col pins as outputs
        GPIO.setup(COL[j], GPIO.OUT)
        GPIO.output(COL[j], 1)

    for i in range (4): # set row pins as inputs
        GPIO.setup(ROW[i], GPIO.IN, pull_up_down = GPIO.PUD_UP)

    # create key variable to store key input and password
    pw = '*' # default with char value to make string
    
    try:
        # variable used to give user 15 seconds of input time
        startTime = datetime.datetime.now() + datetime.timedelta(0, 10)
        #print('Enter numbers ')
        # loop to get user input, MAX 4 digits
        while ((datetime.datetime.now() < startTime) and \
               (len(pw) < (digits + 1))):
            for j in range (4): # loop the cols
                GPIO.output(COL[j], 0) # set output pins low   
                for i in range (4): # loop the rows
                    if GPIO.input(ROW[i]) == 0:
                        print (MATRIX[i][j]) # print values enter by user
                        time.sleep(.3) # pause for .2 of sec
                        key = MATRIX[i][j]  # assigns key pressed
                        pw += key   # append key to matrix
                GPIO.output(COL[j], 1) # set output pins high
    except KeyboardInterrupt: # if ctrl+c
        GPIO.cleanup()
    
    GPIO.cleanup()
    if digits == 1 :
        return pw[1:2] # return 1-digit
    else:
        return pw[1:] # return 4-digit value enter by user

#print(getKeypad())