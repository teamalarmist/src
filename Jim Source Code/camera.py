from picamera import PiCamera # camera functions
from time import sleep # sleep function
import datetime # for now() and strftime() functions
import os # navigating the file directory

# function capture_photo()
# Checks to see if folder CamPhoto exists in the Pictures folder
# If folder does not exist, creates the folder and proceeds
# Creates a camera object and captures a photo. Photo is placed
# into CamPhoto folder with datetime stamp at the end of file
# name. Date format Year_Month_Day_Hour_Min_Sec.

def capture_photo():
    # path to directory on Pi
    directory = os.path.dirname('/home/pi/Pictures/CamPhotos/')
    
    # if the directory does not exist, create it
    try:
        os.makedirs(directory)
    except OSError:
        pass
        
    # camera object
    camera = PiCamera()

    # open object and start a preview of picture that will be taken
    camera.start_preview()
    
    # pause to allow camera to adjust lighting
    sleep(2)

    # capture photo and place in file
    camera.capture(directory + '/Captured_Image_' \
                   + datetime.datetime.now().strftime('%Y%m%d_%H%M%S') + '.jpg')
    #print(datetime.datetime.now().strftime('%Y%m%d_%H%M%S') 

    # close camera object
    camera.stop_preview()
    
# call capture_photo() function to take picture
capture_photo() 

