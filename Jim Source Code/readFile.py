#!/usr/bin/python3
# checkPasscode is a function that accepts a four character string
# opens the passcode text file and compares with the value stored
# in the file with the argument. Returns a boolean value.
import os

def checkPasscode (pin) :
    # open file
    file = open ('/home/pi/Documents/passcode.txt', 'r')
    # variable to return
    success = False
    # attempt to open file and reada
    try:
        x = file.read(4)
    except (IOError, FileNotFoundError):
        print ('Unable to open file. Error Code ')
    else:
        file.close()
    # if matches    
    if pin == x:
        success = True
    # return value
    return success