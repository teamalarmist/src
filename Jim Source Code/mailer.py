# Send email with attachment and date/time stamp of file
# being passed in. 'filename' is the pathname and file.
  
# libraries to be imported 
import smtplib
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
import datetime

#import os
#os.chdir('/home/pi/LabFinal/Mailer/')
#from get_latest_file import getLatestFile

fromaddr = "from@gmail.com"
toaddr = "to@gmail.com"
def emailAlert (filename):  
    # instance of MIMEMultipart 
    msg = MIMEMultipart() 
      
    # storing the senders email address   
    msg['From'] = fromaddr 
      
    # storing the receivers email address  
    msg['To'] = toaddr 
      
    # storing the subject  
    msg['Subject'] = "Intrusion Alert!!!"
      
    # string to store the body of the mail 
    body = "Alarm was triggered on " + \
           datetime.datetime.now().strftime('%m-%d-%Y at %H:%M:%S')
      
    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 
    
    # open the file to be sent  
    #filename = path
    attachment = open(filename, "rb") 
      
    # instance of MIMEBase and named as p 
    p = MIMEBase('application', 'octet-stream') 
      
    # To change the payload into encoded form 
    p.set_payload((attachment).read()) 
      
    # encode into base64 
    encoders.encode_base64(p) 
       
    p.add_header('Content-Disposition', "attachment; filename= %s" % filename) 
      
    # attach the instance 'p' to instance 'msg' 
    msg.attach(p) 
      
    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 
      
    # start TLS for security 
    s.starttls() 
      
    # Authentication 
    s.login(fromaddr, "fromPassword") 
      
    # Converts the Multipart msg into a string 
    text = msg.as_string() 
      
    # sending the mail 
    s.sendmail(fromaddr, toaddr, text) 
      
    # terminating the session 
    s.quit()
    
emailAlert('file to be sent as attachment including file path')