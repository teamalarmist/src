#!/usr/bin/python3
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client


# Your Account Sid and Auth Token from twilio.com/console
# DANGER! This is insecure. See http://twil.io/secure
account_sid = 'AC71e63fdcfae8c1f3b287c731f69992bb'
auth_token = '472b1a83c8983122608edccfbc19872b'
client = Client(account_sid, auth_token)

message = client.messages \
                .create(
                     body="SOMEONE UNAUTHORIZED IS TRYING TO GET IN!",
                     from_='+19162492901',
                     to='+19164718203'
                 )

print(message.sid)