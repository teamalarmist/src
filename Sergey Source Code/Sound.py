#!/usr/bin/python3
#must have pygame library
import pygame

sound="Burglar-alarm-sound.mp3"
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load(sound)
try:
    #plays the file, and restarts when over
    pygame.mixer.music.play(-1) 
    while(1):
        pass
except KeyboardInterrupt:
    pygame.mixer.music.stop()
    pass