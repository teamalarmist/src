#!/usr/bin/python3
#require PIRC522 Library
from pirc522 import RFID
import signal
import time
import sys

rdr = RFID()
util = rdr.util()
#util debug will print whats happening
util.debug = True
#Opens file that lists all Accepted Tags
file=open("AcceptedTags.txt","r")
AcceptedIDs=file.read().splitlines() #reads lines into array
file.close()

run = True
def ReadTag(ID):  # Function for reading the RFID tag
    print("Starting")
    while run:
        #waits for tag to be presented
        rdr.wait_for_tag()
        (error, data) = rdr.request()
        if not error: 
            print("\nDetected: " + format(data, "02x"))
            (error, uid) = rdr.anticoll()
            if not error:
                print("Card read UID: ["+str(uid[0])+", "+str(uid[1])+", "+str(uid[2])+", "+str(uid[3])+", "+str(uid[4])+"]") #prints the UID of the RFID
                ID=(str(uid[0])+" "+str(uid[1])+" "+str(uid[2])+" "+str(uid[3])+" "+str(uid[4]))
                util.set_tag(uid)
                util.deauth()
                break
    return ID


ID=None
#lists accepted tags
print(AcceptedIDs)
try:
    while True:
        print("Reading Tag\n")
        #calls function to read the tag
        ID=ReadTag(ID)
        print("\nThe ID I read is "+str(ID))
        if ID in AcceptedIDs:
            print("ACCEPTED, YOU MAY ENTER")
        else:
            print("DENIED, GET OUT OF HERE")
        time.sleep(1)
except KeyboardInterrupt:
    pass



