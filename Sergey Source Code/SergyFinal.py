#!/usr/bin/python3
from twilio.rest import Client
from pirc522 import RFID
from socket import *
from adafruit_servokit import ServoKit
import http.client, urllib
import _thread
import time
import sys
import pygame
serverName = '10.117.134.51' #change dependning on andrew's server ip
serverPort = 61337 

armed = 0
access = 0
alarm = 0
signal = "empty"

sound="Burglar-alarm-sound.mp3"
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load(sound)

kit = ServoKit(channels=16)

clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((serverName,serverPort))

rdr = RFID()
util = rdr.util()
#util debug will print whats happening
util.debug = True

def SMS():
    account_sid = 'AC71e63fdcfae8c1f3b287c731f69992bb'
    auth_token = '472b1a83c8983122608edccfbc19872b'
    client = Client(account_sid, auth_token)

    message = client.messages \
                .create(
                     body="SOMEONE UNAUTHORIZED IS TRYING TO GET IN!",
                     from_='+19162492901',
                     to='+19164718203'
                 )

    print(message.sid)

def ReadTag(ID):  # Function for reading the RFID tag
    print("Starting")
    while 1:
        #waits for tag to be presented
        rdr.wait_for_tag()
        (error, data) = rdr.request()
        if not error: 
            print("\nDetected: " + format(data, "02x"))
            (error, uid) = rdr.anticoll()
            if not error:
                print("Card read UID: ["+str(uid[0])+", "+str(uid[1])+", "+str(uid[2])+", "+str(uid[3])+", "+str(uid[4])+"]") #prints the UID of the RFID
                ID=(str(uid[0])+" "+str(uid[1])+" "+str(uid[2])+" "+str(uid[3])+" "+str(uid[4]))
                util.set_tag(uid)
                util.deauth()
                break
    return ID
def Pushover():
    conn = http.client.HTTPSConnection("api.pushover.net:443")
    conn.request("POST", "/1/messages.json",
      urllib.parse.urlencode({
        "token": "aqn1b9iv89n2vyeupjm7ca8f4qc2ed",
        "user": "uzkrw1hBj8vdHnwAR9RD2chxAUDBbC",
        "message": "SOMEONE UNAUTHORIZED IS TRYING TO GET IN!",
      }), { "Content-type": "application/x-www-form-urlencoded" })
    conn.getresponse()

def RFID_Read(cSoc):
    ID = None
    while True:
        print("Reading Tag\n")
        #calls function to read the tag
        ID=ReadTag(ID)
        print("\nThe ID I read is "+str(ID))
        if ID in AcceptedIDs:
            print("ACCEPTED, YOU MAY ENTER")
            access = 1
            signal='access1'
            cSoc.send(signal.encode())
            time.sleep(1)
            signal='armed1'
            cSoc.send(signal.encode())
            pygame.mixer.music.stop()
            kit.servo[0].angle = 180
            time.sleep(2)
            kit.servo[0].angle = 0
        else:
            print("DENIED, GET OUT OF HERE")
            access = 0
            signal='access0'
            cSoc.send(signal.encode())
        time.sleep(1)
        
file=open("AcceptedTags.txt","r")
AcceptedIDs=file.read().splitlines() #reads lines into array
file.close()
_thread.start_new_thread(RFID_Read, (clientSocket,))

try:
    while True:
        signal = clientSocket.recv(8)
        signal=signal.decode()
        print (signal)
        if signal == 'armed1':
            armed = 1
        elif signal == "armed0":
            armed = 0
            alarm = 0
            pygame.mixer.music.stop()
            kit.servo[0].angle = 180
            time.sleep(2)
            kit.servo[0].angle = 0
        elif signal == "access1":
            access = 1
            kit.servo[0].angle = 180
            time.sleep(2)
            kit.servo[0].angle = 0
        elif signal == "access0":
            access = 1
        elif signal == "alarm1":
            print ("alarm engaged")
            alarm = 1
            pygame.mixer.music.play(-1) 
            SMS()
            Pushover()
        elif signal == "alarm0":
            print ("alarm disengaged")
            alarm = 0
            pygame.mixer.music.stop() 
        signal = ""
except KeyboardInterrupt:
    clientSocket.close()
