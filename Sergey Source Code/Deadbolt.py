#!/usr/bin/python3
import time
#uses adafruit servokit api to send command to motor controller
from adafruit_servokit import ServoKit
kit = ServoKit(channels=16)
#rotates motor at channel 0 180 degrees then back to 0
kit.servo[0].angle = 180
time.sleep(2)
kit.servo[0].angle = 0

