import RPi.GPIO as GPIO
from socket import * 
import spidev
import thread
import threading
import time
serverName = '10.117.134.51'
serverPort = 61337 

sframe = [0x00, 0x00, 0x00, 0x00]
rframe = [0x00, 0x00, 0x00, 0x00]
eframe = [0x00, 0x00, 0x00]

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

lock = threading.Lock()

armed = 0
accessG = 0
accessD = 0
alarm = 0

def main():
	server_setup()
	
def server_setup():
	clientSocket = socket(AF_INET, SOCK_STREAM)
	clientSocket.connect((serverName,serverPort))
	
	thread.start_new_thread(msgIn,(clientSocket,))
	thread.start_new_thread(windoor,(clientSocket,))
	#thread.start_new_thread(LED_array,())
	while True:
		pass
	clientSocket.close()

def msgIn(cSoc):

	spi = spidev.SpiDev()
	spi.open(0,0)
	spi.no_cs = True
	spi.max_speed_hz = 3000000
	spi.mode = 0

	global alarm
	global armed
	global accessG
	global accessD
	
	thread.start_new_thread(LED_yellow,(spi,))
	
	print "msgIn running\n"
	signal = ""
	while True:
		signal = cSoc.recv(8)
		signal.decode()
		#lock.acquire()
		print "signal is ", signal
		if signal == "armed1":
			armed = 1
			print "armed = ", armed
		elif signal == "armed0":
			armed = 0
			alarm = 0
			print "armed = ", armed
		elif signal == "access1":
			thread.start_new_thread(LED_green,(spi,))
			#LED_green(spi)
			print "accessG = ", accessG
		elif signal == "access0":
			thread.start_new_thread(LED_red,(spi,))
			#LED_red(spi)
			print "accessD = ", accessD
		elif signal == "alarm1":
			#lock.acquire()
			alarm = 1
			#lock.release()
			print "alarm = ", alarm
		elif signal == "alarm0":
			#lock.acquire()
			alarm = 0
			#lock.release()
			print "alarm = ", alarm 
		signal = ""
		#lock.release()

def LED_array():
	spi = spidev.SpiDev()
	spi.open(0,0)
	spi.no_cs = True
	spi.max_speed_hz = 3000000
	spi.mode = 0
	
	global accessD
	global accessG
	thread.start_new_thread(LED_yellow,(spi,))
	#LED_yellow(spi)
	
	while True:
		if accessG == 1:
			lock.acquire()
			thread.start_new_thread(LED_green,(spi,))
			#LED_green(spi)
			time.sleep(2)
			thread.start_new_thread(LED_yellow,(spi,))
			#LED_yellow(spi)
			accessG = 0
			lock.release()
		elif accessD == 1:
			lock.acquire()
			thread.start_new_thread(LED_red,(spi,))
			#LED_red(spi)
			time.sleep(2)
			thread.start_new_thread(LED_yellow,(spi,))
			#LED_yellow(spi)
			accessD = 0
			lock.release()
			
	
		
		
def windoor(cSoc):
	global alarm
	op = 1

	
	print "windoor running\n"
	while True:
		#lock.acquire()
		input_state = GPIO.input(18)
		if input_state == False and op == 1:
			#lock.acquire()
			print 'Window Closed\n'
			#lock.release()
			op = 0
		elif input_state == True and armed == 1 and alarm == 0:
			#lock.acquire()
			op = 1
			signal = "alarm1"
			#lock.acquire()
			alarm = 1
			#lock.release()
			print "alarm triggered!\n"
			#lock.release()
			cSoc.sendall(signal.encode())
		elif input_state == True and op == 0:
			#lock.acquire()
			print 'Window Open\n'
			#lock.release()
			op = 1
		#lock.release()
			
def LED_red(spi):
	count = 0
	spi.writebytes2(sframe)
	while (count < 30):
		spi.writebytes2([0xE5, 0x00, 0x00, 0xFF])
		count += 1
	spi.writebytes2(rframe)
	spi.writebytes2(eframe)
	count = 0
	spi.writebytes2(sframe)
	while (count < 30):
		spi.writebytes2([0xE5, 0x00, 0xFF, 0xFF])
		count += 1
	spi.writebytes2(rframe)
	spi.writebytes2(eframe)
	
def LED_yellow(spi):
	count = 0
	spi.writebytes2(sframe)
	while (count < 30):
		spi.writebytes2([0xE5, 0x00, 0xFF, 0xFF])
		count += 1
	spi.writebytes2(rframe)
	spi.writebytes2(eframe)
	
	
def LED_green(spi):
	count = 0
	spi.writebytes2(sframe)
	while (count < 30):
		spi.writebytes2([0xE5, 0x00, 0xFF, 0x00])
		count += 1
	spi.writebytes2(rframe)
	spi.writebytes2(eframe)
	count = 0
	spi.writebytes2(sframe)
	while (count < 30):
		spi.writebytes2([0xE5, 0x00, 0xFF, 0xFF])
		count += 1
	spi.writebytes2(rframe)
	spi.writebytes2(eframe)
	
			
	
main()


