
from socket import * 
import thread
import threading

lock = threading.Lock()

def main():
	start_server()
	
def start_server():
	serverPort =  61337 
	sSoc = socket(AF_INET,SOCK_STREAM)

	sSoc.bind(('10.117.134.51', serverPort))
	print 'Server started!'

	sSoc.listen(4)
	print 'Server listening!'
	try:
		while True:
			c1, addr1 = sSoc.accept()
			print 'Pi1 connected!'
			c2, addr2 = sSoc.accept()
			print 'Pi2 connected!'
			c3, addr3 = sSoc.accept()
			print 'Pi3 connected!'

			thread.start_new_thread(new_client1,(c1,c2,c3,addr1))
			thread.start_new_thread(new_client2,(c1,c2,c3,addr2))
			thread.start_new_thread(new_client3,(c1,c2,c3,addr3))
	except KeyboardInterrupt:
		c1.close()
		c2.close()
		c3.close()
		sSoc.close()

def new_client1(csoc1,csoc2,csoc3,addrs):
	msg = ""
	while True:
		print "thread1"
		msg = csoc1.recv(8)
		#lock.acquire()
		print "thread 1 msg got"
		if msg != "":
			print addrs, ' >> ', msg
			csoc2.send(msg)
			csoc3.send(msg)
		msg = ""
		#lock.release()

def new_client2(csoc1,csoc2,csoc3,addrs):
	msg = ""
	while True:
		print "thread2"
		msg = csoc2.recv(8)
		#lock.acquire()
		print "thread 2 msg got"
		if msg != "":
			print addrs, ' >> ', msg
			csoc1.send(msg)
			csoc3.send(msg)
		msg = ""
		#lock.release()

def new_client3(csoc1,csoc2,csoc3,addrs):
	msg = ""
	while True:
		print "thread3"
		msg = csoc3.recv(8)
		#lock.acquire()
		print "thread 3 msg got"
		if msg != "":
			print addrs, ' >> ', msg
			csoc1.send(msg)
			csoc2.send(msg)
		msg = ""
		#lock.release()

main()
